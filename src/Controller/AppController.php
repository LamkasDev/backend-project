<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AppController extends AbstractController {

    /**
      * @Route("/")
      */
    public function renderIndex() : Response {
        return $this->render('index.html.twig', [ "config" => self::getConfig() ]);
    }

    /**
      * @Route("/login")
      */
    public function renderLogin() : Response {
        return $this->render('login.html.twig', [ "errorCode" => "" ]);
    }

    /**
      * @Route("/register")
      */
      public function renderRegister() : Response {
        return $this->render('register.html.twig', [ "errorCode" => "" ]);
    }

    /**
      * @Route("/admin")
      */
      public function renderAdmin(Request $request) : Response {
        $conn = $this->connectDatabase();
        $session = self::getSession($request);

        if($session != null) {
            return $this->render('admin.html.twig', [ "config" => self::getConfig(), "errorCode" => "", "successCode" => "" ]);
        } else {
            return new Response("Unauthorized");
        }
    }

    /**
      * @Route("/deletePortfolio.php")
      */
      public function handleDeletePortfolio(Request $request) : Response {
        $conn = $this->connectDatabase();
        $session = self::getSession($request);

        if($session != null) {
            $id = $request->query->get('id');
            $stmt = $conn->prepare('DELETE FROM portfolio WHERE id = ?');
            $stmt->bind_param('s', $id);
            $stmt->execute();

            return $this->render('admin.html.twig', [ "config" => self::getConfig(), "errorCode" => "", "successCode" => "" ]);
        } else {
            return new Response("Unauthorized");
        }
    }

    /**
      * @Route("/editPortfolio.php")
      */
      public function handleEditPortfolio(Request $request) : Response {
        $conn = $this->connectDatabase();
        $session = self::getSession($request);

        if($session != null) {
            $id = $request->request->get('id');
            $name = utf8_decode($request->request->get('name'));
            $text = utf8_decode($request->request->get('text'));
            $img = $request->request->get('img');
            
            $stmt = $conn->prepare('UPDATE portfolio SET name=?, text=?, img=? WHERE id=?');
            $stmt->bind_param('ssss', $name, $text, $img, $id);
            $stmt->execute();

            return $this->render('admin.html.twig', [ "config" => self::getConfig(), "errorCode" => "", "successCode" => "" ]);
        } else {
            return new Response("Unauthorized");
        }
    }

    /**
      * @Route("/addPortfolio.php")
      */
      public function handleAddPortfolio(Request $request) : Response {
        $conn = $this->connectDatabase();
        $session = self::getSession($request);

        if($session != null) {
            $id = uniqid("", true);
            $name = utf8_decode($request->request->get('name'));
            $text = utf8_decode($request->request->get('text'));
            $img = $request->request->get('img');
            
            $stmt = $conn->prepare('INSERT IGNORE portfolio (name, text, img, id) VALUES (?,?,?,?)');
            $stmt->bind_param('ssss', $name, $text, $img, $id);
            $stmt->execute();

            return $this->render('admin.html.twig', [ "config" => self::getConfig(), "errorCode" => "", "successCode" => "" ]);
        } else {
            return new Response("Unauthorized");
        }
    }

    /**
      * @Route("/handleLogin.php")
      */
      public function handleLogin(Request $request) : Response {
        $conn = $this->connectDatabase();
        $username = $request->request->get('username');
        $password = $request->request->get('password');

        $stmt = $conn->prepare('SELECT * FROM users WHERE username = ?');
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        
        if($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            if(password_verify($password, $row["password"])) {
                $sessionID = self::createSession($row["id"]);

                $response = $this->render('admin.html.twig', [ "username" => $row["username"], "config" => self::getConfig(), "errorCode" => "", "successCode" => "" ]);
                $cookie = new Cookie('sessionID', $sessionID);
                $response->headers->setCookie($cookie);
                return $response;
            } else {
                return $this->render('login.html.twig', [ "errorCode" => "Wrong Password-" ]);
            }
        } else {
            return $this->render('login.html.twig', [ "errorCode" => "User not found-" ]);
        }
    }

    /**
      * @Route("/handleAdmin.php")
      */
      public function handleAdmin(Request $request) : Response {
        $conn = $this->connectDatabase();
        $session = self::getSession($request);

        if($session != null) {
            $title = utf8_decode($request->request->get('title'));
            $subtitle = utf8_decode($request->request->get('subtitle'));
            $about = utf8_decode($request->request->get('about'));
            $link_fb = $request->request->get('link_fb');
            $link_twitter = $request->request->get('link_twitter');
            $link_linkedin = $request->request->get('link_linkedin');
            $link_dribble = $request->request->get('link_dribble');

            $stmt = $conn->prepare('UPDATE configs SET title=?, subtitle=?, about=?, link_fb=?, link_twitter=?, link_linkedin=?, link_dribble=? WHERE id="default"');
            $stmt->bind_param('sssssss', $title, $subtitle, $about, $link_fb, $link_twitter, $link_linkedin, $link_dribble);
            $stmt->execute();

            $stmt = $conn->prepare('SELECT * FROM users WHERE id = ?');
            $stmt->bind_param('s', $session["userID"]);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();

            return $this->render('admin.html.twig', [ "username" => $row["username"], "config" => self::getConfig(), "errorCode" => "", "successCode" => "Uloženo-" ]);
        } else {
            return new Response("Unauthorized");
        }
    }

    /**
      * @Route("/handleRegister.php")
      */
      public function handleRegister(Request $request) : Response {
        $conn = $this->connectDatabase();
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $password2 = $request->request->get('password2');
        if($password != $password2) {
            return $this->render('register.html.twig', [ "errorCode" => "Passwords don't match-" ]);
        } else if(strlen($password) < 3) {
            return $this->render('register.html.twig', [ "errorCode" => "Password is too short-" ]);
        } else if(strlen($username) < 3) {
            return $this->render('register.html.twig', [ "errorCode" => "Username is too short-" ]);
        }

        $stmt = $conn->prepare('SELECT * FROM users WHERE username = ?');
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        
        if($result->num_rows < 1) {
            $id = self::createUser($username, $password);
            $sessionID = self::createSession($id);

            $response = $this->render('admin.html.twig', [ "username" => $username, "config" => self::getConfig(), "errorCode" => "", "successCode" => "" ]);
            $cookie = new Cookie('sessionID', $sessionID);
            $response->headers->setCookie($cookie);
            return $response;
        } else {
            return $this->render('register.html.twig', [ "errorCode" => "User already exists-" ]);
        }
    }

    public function connectDatabase() : Object {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbName = "backendproject";
        $conn = new \mysqli($servername, $username, $password, $dbName);

        return $conn;
    }

    public function getUserByID($id) {
        $conn = $this->connectDatabase();
        $stmt = $conn->prepare('SELECT * FROM users WHERE id = ?');
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();

        return $row;
    }

    public function createSession($userID) {
        $conn = $this->connectDatabase();
        $id = uniqid("", true);

        $sql = "INSERT INTO sessions (id, userID) VALUES ('{$id}', '{$userID}')";
        $result = $conn->query($sql);

        return $id;
    }

    public function createUser($username, $password) {
        $conn = $this->connectDatabase();
        $id = uniqid("", true);

        $passwordHash = password_hash($password, PASSWORD_BCRYPT);
        $stmt = $conn->prepare('INSERT INTO users (id, username, password) VALUES (?, ?, ?)');
        $stmt->bind_param('sss', $id, $username, $passwordHash);
        $stmt->execute();

        return $id;
    }

    public function getConfig() {
        $conn = $this->connectDatabase();
        $sql = "SELECT * FROM configs WHERE id='default'";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        $row["title"] = utf8_encode($row["title"]);
        $row["subtitle"] = utf8_encode($row["subtitle"]);
        $row["about"] = utf8_encode($row["about"]);
        $row["portfolio"] = [];

        $sql = "SELECT * FROM portfolio";
        $result = $conn->query($sql);
        while($row2 = $result->fetch_assoc()) {
            $row2["name"] = utf8_encode($row2["name"]);
            $row2["text"] = utf8_encode($row2["text"]);
            array_push($row["portfolio"], $row2);
        }

        $portolioHTML = '';
        foreach ($row["portfolio"] as $row2) {
            $portolioHTML .= $this->renderView('portfolio.html.twig', [ "data" => $row2 ]);
        }
        $portolioAdminHTML = '';
        foreach ($row["portfolio"] as $row2) {
            $portolioAdminHTML .= $this->renderView('portfolio.admin.html.twig', [ "data" => $row2 ]);
        }

        $row["portfolioLength"] = count($row["portfolio"]);
        $row["portfolioHTML"] = $portolioHTML;
        $row["portfolioAdminHTML"] = $portolioAdminHTML;

        return $row;
    }

    public function getSession($request) {
        $conn = $this->connectDatabase();
        $sessionID = $request->cookies->get("sessionID");
        $stmt = $conn->prepare('SELECT * FROM sessions WHERE id = ?');
        $stmt->bind_param('s', $sessionID);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->num_rows > 0 ? $result->fetch_assoc() : null;
    }

}
?>