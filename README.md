<div align="center">
<h1>Back-end Administration</h1>
<hr />
<h5>By LamkasDev, made with React, Sass</h5>
<img width="6%" src="https://github.com/symfony.png" />
</div>
<hr />

#### How to install:
1) Run <code>composer install</code> in main directory

#### How to run:
1) Run <code>symfony server:start</code>

#### Default credentials:
- Username: <code>admin</code> / Password: <code>admin</code>

#### Database structure file:
Link: <a href="https://files.catbox.moe/emb3vs.sql">Download</a>

<hr />

#### Preview:
<img src="https://femboylamkas.please-fuck.me/mPTk9U.png" />
